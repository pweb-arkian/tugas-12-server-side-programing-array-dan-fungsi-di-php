<!DOCTYPE html>
<html>
<head>
    <title>PHP Array dan Fungsi 1</title>
    <link rel="stylesheet" type="text/css" href="costum.css">
</head>
<body>

<div class="container">
    <h2>Mendeklarasikan & Menampilkan Array</h2>
    <?php
    $arrBuah = array("Mangga", "Apel", "Pisang", "Jeruk");
    echo "Siswa pertama: " . $arrBuah[0] . "<br>"; // Mangga
    echo "Siswa terakhir: " . $arrBuah[3] . "<br><br>"; // Jeruk

    $arrWarna = array();
    $arrWarna[] = "Merah";
    $arrWarna[] = "Biru";
    $arrWarna[] = "Hijau";
    $arrWarna[] = "Putih";
    echo "Warna pertama: " . $arrWarna[0] . "<br>"; // Merah
    echo "Warna ketiga: " . $arrWarna[2] . "<br>"; // Hijau
    ?>
</div>

<div class="container">
    <h2>Array Asosiatif</h2>
    <?php
    $arrNilai = array("Asep" => 80, "Ridho" => 90, "Rohmat" => 75, "Arkyan" => 85);
    echo "Nilai Ridho: " . $arrNilai['Ridho'] . "<br>"; // 90
    echo "Nilai Rohmat: " . $arrNilai['Rohmat'] . "<br><br>"; // 75

    $arrNilai = array();
    $arrNilai['Slamet'] = 80;
    $arrNilai['Waluyo'] = 95;
    $arrNilai['Yanto'] = 77;
    echo "Nilai Waluyo: " . $arrNilai['Waluyo'] . "<br>"; // 95
    echo "Nilai Slamet: " . $arrNilai['Slamet'] . "<br>"; // 80
    ?>
</div>

<div class="container">
    <h2>Menampilkan Seluruh Isi Array dengan FOR dan FOREACH</h2>
    <?php
    $arrWarna = array("Red", "Orange", "Yellow", "Green", "Blue", "Purple");

    echo "Menampilkan isi array dengan <b>FOR</b>:<br>";
    for ($i = 0; $i < count($arrWarna); $i++) {
        echo "<span style='color:" . $arrWarna[$i] . "'>" . $arrWarna[$i] . "</span><br>";
    }

    echo "<br>Menampilkan isi array dengan <b>FOREACH</b>:<br>";
    foreach ($arrWarna as $warna) {
        echo "<span style='color:" . $warna . "'>" . $warna . "</span><br>";
    }
    ?>
</div>

<div class="container">
    <h2>Menampilkan Seluruh Isi Array Asosiatif dengan FOREACH & WHILE-LIST</h2>
    <?php
    $arrNilai = array("Fulan" => 80, "Fulin" => 90, "Fulun" => 75, "Falan" => 85);

    echo "Menampilkan isi array asosiatif dengan <b>Foreach</b>:<br>";
    foreach ($arrNilai as $nama => $nilai) {
        echo "Nilai $nama = $nilai<br>";
    }

    reset($arrNilai);
    echo "<br>Menampilkan isi array asosiatif dengan <b>WHILE dan LIST</b>:<br>";
    while (list($nama, $nilai) = each($arrNilai)) {
        echo "Nilai $nama = $nilai<br>";
    }
    ?>
</div>

</body>
</html>
