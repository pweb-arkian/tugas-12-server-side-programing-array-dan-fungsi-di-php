<!DOCTYPE html>
<html>
<head>
    <title>PHP Array dan Fungsi 2</title>
    <link rel="stylesheet" type="text/css" href="costum.css">
</head>
<body>

<div class="container">
    <h2>Mencetak Struktur Array</h2>
    <?php
    $arrWarna = array("Blue", "Black", "Red", "Yellow", "Green");
    $arrNilai = array("Wahyu" => 80, "Dwi" => 90, "Ihsan" => 75, "Rizal" => 85);

    echo "<pre>";
    print_r($arrWarna);
    echo "<br>";
    print_r($arrNilai);
    echo "</pre>";
    ?>
</div>

<div class="container">
    <h2>Mengurutkan Array dengan sort() dan rsort()</h2>
    <?php
    $arrNilai = array("Budi" => 80, "Agus" => 90, "Romi" => 75, "Fauzan" => 85);

    echo "Array<b> sebelum </b>diurutkan";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    sort($arrNilai);
    reset($arrNilai);
    echo "Array<b> setelah</b> diurutkan dengan <b>sort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    rsort($arrNilai);
    reset($arrNilai);
    echo "Array<b> setelah </b>diurutkan dengan <b>rsort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
    ?>

</div>

<div class="container">
    <h2>Mengurutkan Array dengan asort() dan arsort()</h2>
    <?php
    $arrNilai = array("Imam" => 80, "Andi" => 90, "Ahmad" => 75, "Aziz" => 85);

    echo "Array<b> sebelum </b>diurutkan";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    asort($arrNilai);
    reset($arrNilai);
    echo "Array<b> setelah </b>diurutkan dengan <b>asort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    arsort($arrNilai);
    reset($arrNilai);
    echo "Array<b> setelah </b>diurutkan dengan <b>arsort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
    ?>

</div>

<div class="container">
    <h2>Mengurutkan Array dengan ksort() dan krsort()</h2>
    <?php
    $arrNilai = array("Dyas" => 80, "Syarif" => 90, "Gilang" => 75, "Ibnu" => 85);

    echo "Array<b> sebelum </b>diurutkan";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    ksort($arrNilai);
    reset($arrNilai);
    echo "Array<b> setelah </b>diurutkan dengan <b>ksort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    krsort($arrNilai);
    reset($arrNilai);
    echo "Array<b> setelah </b>diurutkan dengan <b>krsort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
    ?>

</div>

<div class="container">
    <h2>Mengatur Posisi Pointer dalam Array</h2>
    <?php
    $transport = array('Jalan kaki', 'Onthel', 'Mobil', 'Pesawat');
    echo "<pre>";
    print_r($transport);
    echo "</pre>";

    $mode = current($transport);
    echo $mode . "<br>";

    $mode = next($transport);
    echo $mode . "<br>";

    $mode = current($transport);
    echo $mode . "<br>";

    $mode = prev($transport);
    echo $mode . "<br>";

    $mode = end($transport);
    echo $mode . "<br>";

    $mode = current($transport);
    echo $mode . "<br>";
    ?>

</div>

</body>
</html>
