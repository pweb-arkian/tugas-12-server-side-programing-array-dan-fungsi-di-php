<!DOCTYPE html>
<html>
<head>
    <title>PHP Array dan Fungsi 3</title>
    <link rel="stylesheet" type="text/css" href="costum.css">
</head>
<body>

<div class="container">
    <h2>Mencari elemen array</h2>
    <?php
    $arrBuah = array("Mangga", "Apel", "Pisang", "Kedondong", "Jeruk");
    if (in_array("Kedondong", $arrBuah)) {
        echo "Ada buah Kedondong di dalam array tsb!";
    } else {
        echo "Tidak ada buah Kedondong di array tersebut";
    }
    ?>
</div>

<div class="container">
    <h2>Fungsi tanpa return value & parameter</h2>
    <?php
    // Fungsi ini tanpa return value, & tanpa parameter
    function cetak_ganjil_tanpa_parameter() {
        for ($i = 0; $i < 100; $i++) {
            if ($i % 2 == 1) {
                echo "$i, ";
            }
        }
    }
    // Pemanggilan fungsi
    cetak_ganjil_tanpa_parameter();
    ?>
</div>

<div class="container">
    <h2>Fungsi tanpa return value tapi dengan parameter</h2>
    <?php
    // Fungsi ini tanpa return value, & parameter
    function cetak_ganjil_dengan_parameter($awal, $akhir) {
        for ($i = $awal; $i < $akhir; $i++) {
            if ($i % 2 == 1) {
                echo "$i, ";
            }
        }
    }
    // Pemanggilan fungsi
    $a = 10;
    $b = 50;
    echo "<b>Bilangan ganjil dari $a sampai $b, adalah:</b><br>";
    cetak_ganjil_dengan_parameter($a, $b);
    ?>
</div>

<div class="container">
    <h2>Fungsi dengan return value & parameter</h2>
    <?php
    // Fungsi dengan return value dan parameter
    function luas_lingkaran($jari) {
        return 3.14 * $jari * $jari;
    }
    // Pemanggilan fungsi
    $r = 10;
    echo "Luas Lingkaran dengan jari-jari $r = ";
    echo luas_lingkaran($r);
    ?>
</div>

<div class="container">
    <h2>Passing by value</h2>
    <?php
    function tambah_string($str) {
        $str = $str . ", Yogyakarta";
        return $str;
    }
    $string = "Universitas Ahmad Dahlan";
    echo "\$string = $string<br>";
    echo tambah_string($string) . "<br>";
    echo "\$string = $string<br>";
    ?>
</div>

<div class="container">
    <h2>Passing by reference</h2>
    <?php
    function tambah_string_reference(&$str) {
        $str = $str . ", Yogyakarta";
        return $str;
    }
    $string = "Universitas Ahmad Dahlan";
    echo "\$string = $string<br>";
    echo tambah_string_reference($string) . "<br>";
    echo "\$string = $string<br>";
    ?>
</div>

<div class="container">
    <h2>Tampilkan UDF dan fungsi yang didukung PHP versi saat ini</h2>
    <?php
    $arr = get_defined_functions();
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
    ?>
</div>

</body>
</html>